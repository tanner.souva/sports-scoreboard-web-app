const mongoose = require('mongoose');

const GameSchema = new mongoose.Schema({
  gameID: {
    type: String,
    required: true
  },
  homeName: {
    type: String,
    required: true
  },
  homeScore: {
    type: Number,
    required: true
  },
  awayName: {
    type: String,
    required: true
  },
  awayScore: {
    type: Number,
    required: true
  },
  division: {
    type: String,
    required: true
  },
  period: {
    type: Number,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
});

const Game = mongoose.model('Game', GameSchema);

module.exports = Game;