const { Logger } = require('danno-tools')
const logger = new Logger({ WTF: true, path: './logs' })

module.exports = {
  ensureAuthenticated: function(req, res, next) {
    if (req.isAuthenticated()) {
      return next();
    }
    req.flash('error_msg', 'Please log in to view that resource');
    res.redirect('/users/login');
  },
  forwardAuthenticated: function(req, res, next) {
    if (!req.isAuthenticated()) {
      return next();
    }
    res.redirect('/dashboard');      
  },
  isAdmin: function(req, res, next) {
    if (req.user.admin) {
      return next();
    }
    logger.warn(`${req.user.email} tried to access an admin only page! ${req.path}`)
    req.flash('error_msg', 'You must be an admin to view this page');
    res.redirect('/dashboard');
  }
};
