const express = require('express');
const router = express.Router();
const fs = require('fs')
const { Logger } = require('danno-tools')
const logger = new Logger({ WTF: true, path: './logs' })
const { ensureAuthenticated, forwardAuthenticated } = require('../config/auth');

// Welcome Page
router.get('/', (req, res) => {
  fs.readFile(`./data.json`,'utf8', (err, data) => {
    if (err) {logger.error(err); return false;}
    let game = JSON.parse(data)
    res.render('welcome', {
      isAuth: req.isAuthenticated(),
      game,
      banner: true
    });
  })
});
router.get('/streamboard', (req, res) => {
  fs.readFile(`./data.json`,'utf8', (err, data) => {
    if (err) {logger.error(err); return false;}
    let game = JSON.parse(data)
    res.render('streamboard', {
      isAuth: req.isAuthenticated(),
      game,
      banner: false
    });
  })
});

// Dashboard
router.get('/dashboard', ensureAuthenticated, async (req, res) => {
  res.render('dashboard', {
    user: req.user,
    isAuth: req.isAuthenticated(),
    banner: true
  })
});

module.exports = router;
