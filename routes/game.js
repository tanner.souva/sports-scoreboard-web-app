const express = require('express');
const router = express.Router();
const Games = require('../models/Game')
const fs = require('fs')
const { Logger } = require('danno-tools')
const logger = new Logger({ WTF: true, path: './logs' })

router.get('/', (req,res) => {
    let oneWeek = new Date();
    oneWeek.setDate(oneWeek.getDate() - 7)
    Games.find({date: {$gte: oneWeek}},(err,games) => {
        res.render('games', {
            user: req.user,
            isAuth: req.isAuthenticated(),
            banner: true,
            games
        });
    });
    
})

router.get('/:id', (req,res) => {
    let gameIDParam = req.params.id;
    if(gameIDParam) {
        Games.findOne({ gameID: gameIDParam }).then(game => {
            if (game) {
                res.render('game', {
                    user: req.user,
                    game,
                    isAuth: req.isAuthenticated(),
                    banner: true
                });
            } else {
                let oneWeek = new Date();
                oneWeek.setDate(oneWeek.getDate() - 7)
                Games.find({date: {$gte: oneWeek}},(err,games) => {
                    res.render('games', {
                        user: req.user,
                        isAuth: req.isAuthenticated(),
                        banner: true,
                        errors: [{ msg: `Game doesn't exist!` }],
                        games
                    });
                });
            }
        })
    }
})

module.exports = router