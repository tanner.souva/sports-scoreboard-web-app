const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const passport = require('passport');
const randomatic = require('randomatic');
// Load User model
const User = require('../models/User');
const { forwardAuthenticated, isAdmin, ensureAuthenticated } = require('../config/auth');
const { Logger } = require('danno-tools')
const logger = new Logger({ WTF: true, path: './logs' })

// Login Page
router.get('/login', forwardAuthenticated, (req, res) => res.render('login', {
  isAuth: req.isAuthenticated(),
  banner: true
}));

// Register Page
router.get('/register', ensureAuthenticated, isAdmin, (req, res) => res.render('register', {
  isAuth: req.isAuthenticated(),
  banner: true
}));

// Register
router.post('/register', (req, res) => {
  var { name, email, password, password2, admin } = req.body;
  admin = (admin === "on")
  let errors = [];

  if (!name || !email || !password || !password2) {
    errors.push({ msg: 'Please enter all fields' });
  }

  if (password != password2) {
    errors.push({ msg: 'Passwords do not match' });
  }

  if (password.length < 6) {
    errors.push({ msg: 'Password must be at least 6 characters' });
  }

  if (errors.length > 0) {
    res.render('register', {
      errors,
      name,
      email,
      password,
      password2,
      isAuth: req.isAuthenticated()
    });
  } else {
    User.findOne({ email: email }).then(user => {
      if (user) {
        errors.push({ msg: 'Email already exists' });
        res.render('register', {
          errors,
          name,
          email,
          password,
          password2
        });
      } else {
        const newUser = new User({
          name,
          email,
          password,
          admin,
          apiKey: randomatic('aA0', 32)
        });

        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser
              .save()
              .then(user => {
                req.flash(
                  'success_msg',
                  `${user.email} registered with ${password}`
                );
                logger.log(`${req.user.email} registered ${user.email}`)
                res.redirect('/dashboard');
              })
              .catch(err => console.log(err));
          });
        });
      }
    });
  }
});

// Login
router.post('/login', (req, res, next) => {
  passport.authenticate('local', {
    successRedirect: '/dashboard',
    failureRedirect: '/users/login',
    failureFlash: true
  })(req, res, next)
});

// Logout
router.get('/logout', (req, res) => {
  logger.log(`${req.user.email} logged out`)
  req.logout();
  req.flash('success_msg', 'You are logged out');
  res.redirect('/users/login');
});

module.exports = router;
