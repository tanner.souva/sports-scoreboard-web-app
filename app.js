const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const passport = require('passport');
const flash = require('connect-flash');
const session = require('express-session');
const fs = require('fs')
const crypto = require('crypto')
const User = require('./models/User');
const Game = require('./models/Game')
const algorithm = 'aes-256-ctr';
const { Logger } = require('danno-tools')
const logger = new Logger(
  {
    WTF: true, 
    path: './logs'
  })
const app = express();
const host = "https://app.sports-scoreboard.live";

// Passport Config
require('./config/passport')(passport);

// DB Config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
  .connect(
    db,
    { 
      dbName: "SportScoreboardAPI", 
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  )
  .then(() => logger.log('MongoDB Connected'))
  .catch(err => console.log(err));

// EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Express body parser
app.use(express.urlencoded({ extended: true }));

// Express session
app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);

// Passport middleware
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Global variables
app.use(function(req, res, next) {
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});
//        SSLCertificateFile "/etc/apache2/certs-sports-scoreboard/cf.crt"
//        SSLCertificateKeyFile "/etc/apache2/certs-sports-scoreboard/cf.key"
// Routes
app.use('/', require('./routes/index.js'));
app.use('/users', require('./routes/users.js'));
app.use('/games', require('./routes/game.js'));

// HTTP
const HTTP_PORT = 80
let httpAPP = express();
httpAPP.all('*', (req, res) => {
  res.redirect(host)
})
const http = require('http').createServer(httpAPP);
http.listen(HTTP_PORT, () => {
  logger.log(`HTTP Running`);
});

const HTTPS_PORT = 443;
var httpsOptions = { 
  key: fs.readFileSync(__dirname + '/certs/cf.key'),
  cert: fs.readFileSync(__dirname + '/certs/cf.crt')};

const https = require('https').createServer(httpsOptions,app);

https.listen(HTTPS_PORT, () => {
  logger.log(`HTTPS Running`);
});

const {Server} = require('socket.io');
let io = new Server(https)

io.on('connection', (socket) => {
  socket.on('scoreChange', data => {
    let {email,hash} = data
    User.findOne({ email: email }).then(user => {
      if (user) {
        let decryptedData = decrypt(hash,user.apiKey)
        let dataObj = JSON.parse(decryptedData)
        io.emit('scoreChange', dataObj);
        fs.writeFile("./data.json", JSON.stringify(dataObj), (error) => {
          if(error) {
              console.log(error);
          }
        });
        changeGameData(dataObj)
        data = dataObj;
      }
    })
  });
});

const decrypt = (hash,secretUserKey) => {
  const decipher = crypto.createDecipheriv(algorithm, secretUserKey, Buffer.from(hash.iv, 'hex'));
  try {
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
    return decrpyted.toString();
  } catch(err) {
    logger.warn(`Error with decryption: ${err}`)
    return false;
  }
};

function changeGameData(gameData) {
  Game.findOne({gameID: gameData.gameID}).then(game => {
    if (game) {
      Game.findOneAndUpdate({
        gameID: gameData.gameID
      },{
          $set: { 
              awayScore: gameData.away.score,
              homeScore: gameData.home.score,
              period: gameData.period
          }
        }, {useFindAndModify: false}).exec()
    } else {
      const newGame = new Game({
        awayName: gameData.away.name,
        awayScore: gameData.away.score,
        gameID: gameData.gameID,
        homeName: gameData.home.name,
        homeScore: gameData.home.score,
        period: gameData.period,
        division: gameData.division
      });
      newGame.save()
    }
  })
}